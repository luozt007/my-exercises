// css
import './index.css';
// js modules
import _ from 'lodash';
import printMe from './print';
// assets
import icon from './icon.jpg';

function component(){
  const element = document.createElement('div');
  element.classList.add('hello');
  element.innerHTML = _.join(['Hello Webpack'], ' ');
  return element;
}

function image(){
  const img = document.createElement('img');
  img.src = icon;
  return img;
}

function button(){
  const btn = document.createElement('button');
  btn.onclick = ()=>{
    printMe();
  };
  btn.innerText = '测试一下';
  return btn;
}

document.body.appendChild(component());
document.body.appendChild(image());
document.body.appendChild(button());
printMe();